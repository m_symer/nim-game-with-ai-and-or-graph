﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Nim
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameEngine game;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_start_Click(object sender, RoutedEventArgs e)
        {
            if (textbox_zapalky.Text != "") { 
                stackpanel_set.Visibility = Visibility.Collapsed;
                game = new GameEngine(Convert.ToInt32(textbox_zapalky.Text));
                textblock_zapalky.Text = game.Number.ToString();
                stackpanel_game.Visibility = Visibility.Visible;
                StartGame();
            }
        }

        private void button_1_Click(object sender, RoutedEventArgs e)
        {
            PlayerPlay(1);
        }

        private void button_2_Click(object sender, RoutedEventArgs e)
        {
            PlayerPlay(2);
        }

        private void PlayerPlay(int n)
        {
            if (n == 1)
            {
                game.Operation1();
            }
            else
            {
                game.Operation2();
            }
            textblock_zapalky.Text = game.Number.ToString();
            if (!WinControl()) {
                game.SetPlayer();
                button_1.IsEnabled = false;
                button_2.IsEnabled = false;
                textblock_info.Text = "Hraje počítač...";
                new Thread(delegate ()
                {
                    AIPlay();
                }).Start();
            }
        }

        private void AIPlay()
        {
            Dispatcher.BeginInvoke(new ThreadStart(delegate
            {
                textblock_pc.Text = "";

            }));
            Thread.Sleep(1000);
            Dispatcher.BeginInvoke(new ThreadStart(delegate
            {
                if (game.AIPlay() == 1)
                {
                    textblock_pc.Text = "Počítač odebral 1 zápalku.";
                }else
                {
                    textblock_pc.Text = "Počítač odebral 2 zápalky.";
                }
                textblock_zapalky.Text = game.Number.ToString();
                if (!WinControl())
                {
                    game.SetPlayer();
                    button_1.IsEnabled = true;
                    button_2.IsEnabled = true;
                    textblock_info.Text = "Jste na tahu.";
                }

            }));
        }

        private bool WinControl()
        {
            if (game.Number <= 0)
            {
                if (game.IsAI) textblock_konec.Text = "Prohrál jsi. Vyhrál počítač."; else textblock_konec.Text = "Vyhrál jsi.";
                stackpanel_game.Visibility = Visibility.Collapsed;
                stackpanel_konec.Visibility = Visibility.Visible;
                button_1.IsEnabled = true;
                button_2.IsEnabled = true;
                return true;
            }
            else
            { return false; }
        }

        private void StartGame()
        {
            if (game.IsAI)
            {
                textblock_info.Text = "Hraje počítač...";
                AIPlay();
            }else
            {
                textblock_info.Text = "Jste na tahu.";
                textblock_pc.Text = "";
            }
        }

        private void button_novaHra_Click(object sender, RoutedEventArgs e)
        {
            stackpanel_konec.Visibility = Visibility.Collapsed;
            stackpanel_set.Visibility = Visibility.Visible;
        }

        private void button_graph_Click(object sender, RoutedEventArgs e)
        {
            GraphWindow window = new GraphWindow(game);
            window.Show();
        }
    }
}
