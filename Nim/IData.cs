﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nim
{
    public interface IData
    {
        bool IsFinal();
        IData Operation1();
        IData Operation2();
        AndOr Evaluate(bool ai);
    }

    public enum AndOr { NOTHING, TRUE, FALSE}
}
