﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nim
{
    public class GameEngine
    {
        public Graph graph { get; set; }

        public int Number { get; set; }
        public bool IsAI { get; set; }

        public GameEngine(int number)
        {
            Number = number;
            SetAI();
            graph = new Graph(new Node(new NodeData(Number), IsAI));
            graph.Vygenerovat();
        }

        //1. operation for player
        public void Operation1()
        {
            Number = Number - 1;
            graph.ActualNode = graph.ActualNode.Nodes[0];
        }

        //2. operation for player
        public void Operation2()
        {
            Number = Number - 2;
            graph.ActualNode = graph.ActualNode.Nodes[1];
        }

        //round of AI game
        public int AIPlay()
        {
            if(graph.ActualNode.Nodes[0].Value == AndOr.TRUE)
            {
                Operation1();
                return 1;
            }else if (graph.ActualNode.Nodes[1].Value == AndOr.TRUE)
            {
                Operation2();
                return 2;
            }else
            {
                if (graph.ActualNode.Nodes[0].Counter > graph.ActualNode.Nodes[1].Counter)
                {
                    Operation1();
                    return 1;
                }else
                {
                    Operation2();
                    return 2;
                }
            }
        }

        //randomly set 1. player
        private void SetAI()
        {
            Random r = new Random();
            int gen = r.Next(1, 3);
            Console.WriteLine(gen);
            IsAI = gen == 1 ? true : false;
        }

        //set player
        public void SetPlayer()
        {
            IsAI = IsAI ? false : true;
        }

        
        
    }
}
