﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Nim
{
    /// <summary>
    /// Interaction logic for GraphWindow.xaml
    /// </summary>
    public partial class GraphWindow : Window
    {
        private GameEngine game;

        public GraphWindow(GameEngine game)
        {
            InitializeComponent();
            this.game = game;
            DrawGraph(game.graph.ActualNode);
        }

        private void DrawGraph(Node node)
        {
            DrawGraph(1, 1, node);
        }

        private void DrawGraph(int level, int branch, Node node)
        {
            int i = 1;
            if (level <= 2)
            {
                foreach (Node n in node.Nodes)
                {
                    DrawGraph(level + 1, ((branch == 1) && (i == 1)) ? 1 : branch + i, n);
                    i++;
                    Console.WriteLine(i + n.Value);
                }
            }
            TextBlock textblock = new TextBlock();
            textblock.Text = (node.IsAI ? "H" : "PC") + " (" + node.Value + "): " + node.Data.ToString();
            textblock.Margin = new Thickness((branch - 1) * 80, 25 * level,0,0);
            textblock.HorizontalAlignment = HorizontalAlignment.Left;
            textblock.Width = 80;
            grid_graf.Children.Add(textblock);
        }

        //nepouzito ale muze se hodit do budoucna
        private int GraphSize(Node node)
        {
            int i = 0;
            int pom = 1;
            foreach (Node n in node.Nodes)
            {
                pom = GraphSize(n);
                if (i < pom) i = pom;
            }
            return i+1;
        }

        //nepouzito ale taky se muze hodit
        private int GraphWidth(int level, Node node)
        {
            int count = 1;
            if (level > 1)
            {
                foreach(Node n in node.Nodes)
                {
                    count = count + GraphWidth(level - 1,n);
                }
            }
            return count;
        }
    }
}
