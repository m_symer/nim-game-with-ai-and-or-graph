﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nim
{
    public class Node
    {
        public List<Node> Nodes { get; set; }
        public AndOr Value { get; set; }
        public IData Data { get; set; }
        public bool IsAI { get; set; }
        public int Counter { get; set; }

        public Node(IData d, bool ai)
        {
            Data = d;
            Counter = 0;
            Value = AndOr.NOTHING;
            IsAI = ai == true ? false : true;
        }

        //generate next nodes
        public void Generate()
        {
            Nodes = new List<Node>();
            if (!Data.IsFinal())
            {
                Nodes.Add(new Node(Data.Operation1(),IsAI));
                Nodes.Add(new Node(Data.Operation2(),IsAI));
                Nodes[0].Generate();
                Nodes[1].Generate();
                Evaluate();
            }
            else {
                Value = Data.Evaluate(IsAI);
            }
        }

        //set value of node
        private void Evaluate()
        {
           if(Nodes.Count != 0)
            {
                Value = ((Nodes[0].Value == AndOr.TRUE) && (Nodes[1].Value == AndOr.TRUE)) ? AndOr.TRUE : AndOr.FALSE;
                if (Value == AndOr.FALSE)
                {
                    if (!IsAI)
                    {
                        Value = ((Nodes[0].Value == AndOr.TRUE) || (Nodes[1].Value == AndOr.TRUE)) ? AndOr.TRUE : AndOr.FALSE;
                    }
                }
                int count = Nodes[0].Counter + Nodes[1].Counter;
                Counter = Value == AndOr.TRUE ? count + 1 : count;
            }else {
                Value = AndOr.NOTHING;
            }
        }

    }
}
