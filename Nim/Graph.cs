﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nim
{
    public class Graph
    {
        public Node ActualNode { get; set; }

        public Graph(Node first)
        {
            ActualNode = first;
        }

        //generate AND-OR graph from actual node
        public void Vygenerovat()
        {
            ActualNode.Generate();
        }
    }
}
