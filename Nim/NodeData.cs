﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nim
{
    class NodeData : IData
    {
        private int value;

        public NodeData(int value)
        {
            this.value = value;
        }

        public AndOr Evaluate(bool ai)
        {
            if (ai)
            {
                return AndOr.TRUE;
            }else
            {
                return AndOr.FALSE;
            }
        }

        public bool IsFinal()
        {
            return value <= 0 ? true : false;
        }

        public IData Operation1()
        {
            return new NodeData(value - 1);
        }

        public IData Operation2()
        {
            return new NodeData(value - 2);
        }

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
