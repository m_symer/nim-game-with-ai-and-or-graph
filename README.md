# README #


### What is this repository for? ###

* Simple game NIM
* School project
* WPF application
* Using AND-OR Graph
* **Game is in czech language.**

### How to play? ###

* Write integer number (max 50 recommended).
* You subtract the numbers 1 or 2.
* The goal is to reach the number 0.
* AI plays versus you. It wants to reach the number 0 too.